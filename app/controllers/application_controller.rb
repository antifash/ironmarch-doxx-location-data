# coding: utf-8
require 'haml'
require 'sinatra/activerecord'
require 'sinatra/base'
require_relative '../helpers/application_helper'

class ApplicationController < Sinatra::Base

  PROJ_ROOT = File.expand_path('../../..', __FILE__)

  def self.load_code
    %w(controllers helpers models views).each do |dir|
      Dir.glob("./app/#{dir}/*.rb").each { |f| require f }
    end
  end

  configure do
    set :haml, { escape_html: true }
    set :root, PROJ_ROOT
    set :server, :puma
    set :show_exceptions, false
    set :static, true
    set :views, File.join(PROJ_ROOT, 'app', 'views')

    helpers ApplicationHelper
  end

  before do
    content_type 'text/html'
  end

  get '/' do
    @mapbox_key = ENV['MAPBOX_KEY']

    osm_url = 'https://www.openstreetmap.org'
    cc_url  = 'https://creativecommons.org/licenses/by-sa/2.0'
    mapbox_url = 'https://www.mapbox.com/'
    @map_attribution = ("Map data &copy; <a href='#{osm_url}'>OpenStreetMap</a> " +
                        "contributors, <a href='#{cc_url}'>CC-BY-SA</a>, " +
                        "Imagery © <a href='#{mapbox_url}'>Mapbox</a>")
    haml :index
  end

end

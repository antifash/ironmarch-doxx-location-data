module ApplicationHelper

  def production?
    ENV['RACK_ENV'] == 'production'
  end

  # Form helpers categorized by function
  %w().each do |name|
    require_relative name
    include name.camelize.constantize
  end

end

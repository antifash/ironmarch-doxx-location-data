class CoreMember < ActiveRecord::Base
  has_one :location, foreign_key: :member_id
end

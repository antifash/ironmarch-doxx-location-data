require 'sinatra/base'

require_relative 'app/controllers/application_controller'
ApplicationController.load_code

run Rack::URLMap.new('/' => ApplicationController)

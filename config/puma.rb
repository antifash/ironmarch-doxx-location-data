root = "#{Dir.getwd}"

threads 0, 32
rackup "#{root}/config.ru"
activate_control_app

### How to use this code

This repository contains some basic ruby code to work with the IronMarch database doxx released by comrades.  The database can be obtained from links on the bellingcat website (see references below).  The source code is very simple and is meant as an easy way to work to write scripts to work with the db programmatically.  `db/dumps/locations.sql` is an interesting table that has geolocation data for member IPs based on a geolookup service.  Obviously will include lots of VPNs / tor exit nodes, but this can be an interesting heat map of access to IronMarch's website.

### Prerequisites
* install mysql
* install rbenv
  - https://github.com/rbenv/rbenv#basic-github-checkout
  - https://github.com/rbenv/ruby-build#installation
  - `rbenv install`
* setup db
  - create `antifa` user with access to blank `fascists` database in mysql
  - import db dumps from `db/dumps` into `fascists` mysql db
  - `cp config/database.yml.example config/database.yml`
  - update values for your user / database if necessary in `config/database.yml`
* install gems
  - `gem install bundler && rbenv rehash && bundle`
* obtain API key for mapbox
  - https://www.mapbox.com/studio/account/tokens/
  - `cp .envrc.example .envrc` (and update with your values)

### References
* https://www.bellingcat.com/resources/how-tos/2019/11/06/massive-white-supremacist-message-board-leak-how-to-access-and-interpret-the-data/
* https://docs.google.com/spreadsheets/d/1mJfnYKqZdwPhK1bFbrx9hCi-61n5f0tener8iZ3AkOw/edit#gid=0

### Appreciation

In addition to the badass comrades who obtained the IronMarch database dump, want to express much love to the following to the following projects for making this tool not only possible, but easy / quick / enjoyable to develop.

* http://sinatrarb.com
* https://ipstack.com/
* https://leafletjs.com
* https://github.com/Leaflet/Leaflet.markercluster
* https://www.mapbox.com
* https://www.openstreetmap.org
* https://github.com/rails/rails/tree/master/activerecord




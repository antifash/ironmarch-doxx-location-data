require 'json'

h = {}
CoreMember.joins(:location).each do |cm|
  key = "#{cm.location.latitude}_#{cm.location.longitude}"
  member_info    = %w(member_id name member_title email ip_address)
  location_info  = %w(country_name city zip latitude longitude)

  h[key] ||= {}
  h[key][cm.member_id] = {}
  member_info.each   { |str| h[key][cm.member_id][str] = cm.send(str) }
  location_info.each { |str| h[key][cm.member_id][str] = cm.location.send(str) }
end

File.write('public/data.js', h.to_json)

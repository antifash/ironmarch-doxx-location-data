require 'json'
require 'rest-client'

ACCESS_KEY = ENV['API_KEY']

CoreMember.all.each do |cm|
  ip_addr = cm.ip_address
  url  = "http://api.ipstack.com/#{ip_addr}?access_key=#{ACCESS_KEY}"
  res = JSON.parse(RestClient.get(url))

  Location.create(
    member_id: cm.member_id,
    continent_name: res['continent_name'],
    country_code: res['country_code'],
    country_name: res['country_name'],
    city: res['city'],
    zip: res['zip'],
    latitude: res['latitude'],
    longitude: res['longitude']
  )
end
